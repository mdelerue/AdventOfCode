import Bunyan from 'bunyan'

const logger = Bunyan.createLogger({ name: 'AOC' })

export const loggerStart = (label: string) => {
  logger.info(`Starting`, label)
}

export const loggerEnd = (label: string, result: Record<string, any>) => {
  logger.info(result, label)
}

export const log = logger
