import fs from 'fs'

export default {
  readInput: async (filePath: string, delimiter = '\n'): Promise<string[]> =>
    new Promise((resolve, reject) => {
      fs.readFile(filePath, { encoding: 'utf-8' }, (err, data) => {
        if (err) {
          reject(err)
        }
        resolve(data.split(delimiter))
      })
    }),
}
