import {
  MatchingTable,
  ProcessDetail,
  formatSeedInput,
  getMatchingTable,
  getSeeds,
  groupInput,
  part1,
  processMatchingTable,
} from './day05'

describe('2023 - Day 05', () => {
  const inputs = [
    'seeds: 79 14 55 13',
    '',
    'seed-to-soil map:',
    '50 98 2',
    '52 50 48',
    '',
    'soil-to-fertilizer map:',
    '0 15 37',
    '37 52 2',
    '39 0 15',
    '',
    'fertilizer-to-water map:',
    '49 53 8',
    '0 11 42',
    '42 0 7',
    '57 7 4',
    '',
    'water-to-light map:',
    '88 18 7',
    '18 25 70',
    '',
    'light-to-temperature map:',
    '45 77 23',
    '81 45 19',
    '68 64 13',
    '',
    'temperature-to-humidity map:',
    '0 69 1',
    '1 0 69',
    '',
    'humidity-to-location map:',
    '60 56 37',
    '56 93 4',
  ]

  const expectedSeeds = [79, 14, 55, 13]

  const expectedFormatedInput: ProcessDetail = {
    79: [79],
    14: [14],
    55: [55],
    13: [13],
  }

  const expectedMatchingTable: MatchingTable = {
    matches: [
      {
        sourceBeginning: 98,
        destinationBeginning: 50,
        range: 2,
      },
      {
        sourceBeginning: 50,
        destinationBeginning: 52,
        range: 48,
      },
    ],
  }

  const expectedProcessedDetail: ProcessDetail = {
    79: [79, 81],
    14: [14, 14],
    55: [55, 57],
    13: [13, 13],
  }

  it('Should get concerned seeds from input', () => {
    const seeds = getSeeds(inputs)

    expect(seeds).toEqual(expectedSeeds)
  })

  it('Should format inputs', () => {
    expect(formatSeedInput(expectedSeeds)).toEqual(expectedFormatedInput)
  })

  it('Should return the matching table to convert seed to oil', () => {
    const table = getMatchingTable(['50 98 2', '52 50 48'])
    expect(table).toEqual(expectedMatchingTable)
  })

  it('Should process input through matching table', () => {
    const result = processMatchingTable(expectedFormatedInput, expectedMatchingTable)
    expect(result).toEqual(expectedProcessedDetail)
  })

  it('Should group input by mapping type', () => {
    const result = groupInput(inputs)
    expect(result.length).toBe(7)
    expect(result[0].matchingTable).toEqual(expectedMatchingTable)
    expect(result.map(({ type }) => type)).toStrictEqual([
      'seed-to-soil',
      'soil-to-fertilizer',
      'fertilizer-to-water',
      'water-to-light',
      'light-to-temperature',
      'temperature-to-humidity',
      'humidity-to-location',
    ])
  })

  it('Should return part1 result', () => {
    expect(part1(inputs)).toBe(35)
  })
})
