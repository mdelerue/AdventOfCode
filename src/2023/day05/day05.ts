import * as path from 'path'
import fileHelper from '../../helpers/file'

const filepath = path.resolve('src/2023/day05/input.txt')

export type MatchObject = {
  sourceBeginning: number
  destinationBeginning: number
  range: number
}
export type MatchingTable = {
  matches: MatchObject[]
}

export type ProcessDetail = Record<number, number[]>

export type FormattedInput = { matchingTable: MatchingTable; type: string }

export const getSeeds = (inputs: string[]): number[] => {
  for (const line of inputs) {
    if (line.includes('seeds: ')) {
      return line
        .replace('seeds: ', '')
        .split(' ')
        .map((seed: string) => parseInt(seed, 10))
    }
  }

  return []
}

export const formatSeedInput = (seeds: number[]): ProcessDetail => {
  return seeds.reduce((detail: ProcessDetail, seed) => ({ ...detail, [seed]: [seed] }), {})
}

export const groupInput = (inputs: string[]): FormattedInput[] => {
  const formattedInput: FormattedInput[] = []
  let currentType = ''
  let currentLineGroup: string[] = []
  const maxIndex = inputs.length - 2 - 1
  for (const line of inputs.slice(2)) {
    if (line.trim() === '') {
      formattedInput.push({
        type: currentType,
        matchingTable: getMatchingTable(currentLineGroup),
      })
      currentType = ''
      currentLineGroup = []
      continue
    }
    if (isNaN(parseInt(line.split(' ')[0], 10))) {
      currentType = line.split(' ')[0]
      continue
    }

    currentLineGroup.push(line)
  }

  formattedInput.push({
    type: currentType,
    matchingTable: getMatchingTable(currentLineGroup),
  })

  return formattedInput
}

export const getMatchingTable = (lines: string[]): MatchingTable => {
  return {
    matches: lines.map((line) => {
      const [destinationBeginning, sourceBeginning, range] = line
        .split(' ')
        .map((item) => parseInt(item, 10))
      return {
        destinationBeginning,
        sourceBeginning,
        range,
      }
    }),
  }
}

export const processMatchingTable = (
  processDetail: ProcessDetail,
  matchingTable: MatchingTable,
): ProcessDetail => {
  return Object.entries(processDetail).reduce((acc: ProcessDetail, [seedId, path]) => {
    const lastElement = path[path.length - 1]

    const destination = matchingTable.matches.reduce((accDest: number | undefined, match) => {
      if (
        lastElement >= match.sourceBeginning &&
        lastElement <= match.sourceBeginning + match.range - 1
      ) {
        return match.destinationBeginning - match.sourceBeginning + lastElement
      }
      return accDest
    }, undefined)

    return {
      ...acc,
      [seedId]: [...path, destination ?? lastElement],
    }
  }, {})
}

export const part1 = (inputs: string[]): number => {
  const processDetail = formatSeedInput(getSeeds(inputs))
  const formattedInputs = groupInput(inputs)
  const finalDetails = formattedInputs.reduce(
    (details, formattedInput) => processMatchingTable(details, formattedInput.matchingTable),
    processDetail,
  )

  const lowest: number =
    Object.entries(finalDetails).reduce((acc: number | undefined, [_key, path]) => {
      const location = path[path.length - 1]
      if (!acc || location < acc) {
        return location
      }

      return acc
    }, undefined) ?? 0
  return lowest
}

export const day05 = async () => {
  const inputs = await fileHelper.readInput(filepath)

  return {
    part1: part1(inputs),
    part2: -1,
  }
}
