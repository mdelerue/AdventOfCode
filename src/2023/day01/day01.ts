import * as path from 'path'

import fileHelper from '../../helpers/file'

const filepath = path.resolve('src/2023/day01/input.txt')

const FIRST_LAST_NUMBER_REGEX = /^\D*(?<first>\d{1}).*(?<last>\d{1})\D*$/
const ONLY_NUMBER_REGEX = /^\D*(?<number>\d{1})\D*$/

const FIRST_LAST_NUMBER_REGEX_PT2 =
  /(?<first>\d|one|two|three|four|five|six|seven|eight|nine).*(?<last>\d|one|two|three|four|five|six|seven|eight|nine).*$/
const ONLY_NUMBER_REGEX_PT2 = /.*(?<number>\d|one|two|three|four|five|six|seven|eight|nine).*$/

const DIGIT_TABLE = {
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
}

export const calculateTopSum = (input: number[][], numberOfElementsToKeep: number): number => {
  const sums = input.reduce((results, elve: number[]) => {
    return [...results, elve.reduce((total, calories) => total + calories, 0)]
  }, [] as number[])

  sums.sort((a, b) => b - a)

  let result = 0
  for (let i = 0; i < numberOfElementsToKeep; i++) {
    result += sums[i]
  }

  return result
}

export const calculateMaxCalories = (input: number[][]): number => {
  return input.reduce((max, elve: number[]) => {
    const sum = elve.reduce((total, calories) => total + calories, 0)
    if (sum > max) {
      return sum
    }
    return max
  }, 0)
}

export const sum = (numbers: number[]): number => {
  return numbers.reduce((sum, num) => sum + num, 0)
}

export const convertInputToInt = (inputNum: string): number => {
  const result = parseInt(inputNum, 10)

  if (!isNaN(result)) {
    return result
  }

  return Object.entries(DIGIT_TABLE).reduce((val, [key, value]) => {
    if (inputNum === key) {
      return value
    }
    return val
  }, NaN)
}

export const calculateCalibrationValuesPt1 = (lines: string[]): number[] => {
  return lines.map((line) => {
    let regexResult = FIRST_LAST_NUMBER_REGEX.exec(line)

    if (regexResult) {
      const resultGroup = { ...regexResult.groups } as { first: string; last: string }
      return parseInt(
        `${convertInputToInt(resultGroup.first)}${convertInputToInt(resultGroup.last)}`,
        10,
      )
    }

    regexResult = ONLY_NUMBER_REGEX.exec(line)

    if (!regexResult) {
      throw new Error('NO NUMBER IN STRING')
    }

    const resultGroup = { ...regexResult.groups } as { number: string }
    return parseInt(
      `${convertInputToInt(resultGroup.number)}${convertInputToInt(resultGroup.number)}`,
      10,
    )
  })
}

export const calculateCalibrationValuesPt2 = (lines: string[]): number[] => {
  return lines.map((line) => {
    let regexResult = FIRST_LAST_NUMBER_REGEX_PT2.exec(line)

    if (regexResult) {
      const resultGroup = { ...regexResult.groups } as { first: string; last: string }
      return parseInt(
        `${convertInputToInt(resultGroup.first)}${convertInputToInt(resultGroup.last)}`,
        10,
      )
    }

    regexResult = ONLY_NUMBER_REGEX_PT2.exec(line)

    if (!regexResult) {
      throw new Error('NO NUMBER IN STRING')
    }

    const resultGroup = { ...regexResult.groups } as { number: string }
    return parseInt(
      `${convertInputToInt(resultGroup.number)}${convertInputToInt(resultGroup.number)}`,
      10,
    )
  })
}

export const day01 = async () => {
  const input = await fileHelper.readInput(filepath)

  return {
    part1: sum(calculateCalibrationValuesPt1(input)),
    part2: sum(calculateCalibrationValuesPt2(input)),
  }
}
