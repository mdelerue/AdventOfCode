import { calculateCalibrationValuesPt1, calculateCalibrationValuesPt2, sum } from './day01'

const inputArrayPt1 = ['1abc2', 'pqr3stu8vwx', 'a1b2c3d4e5f', 'treb7uchet']

const inputArrayPt2 = [
  'two1nine',
  'eightwothree',
  'abcone2threexyz',
  'xtwone3four',
  '4nineeightseven2',
  'zoneight234',
  '7pqrstsixteen',
]

describe('2023 - Day 01', () => {
  it('Should return the sum of all numbers in the array', () => {
    const result = sum([12, 38, 15, 77])
    expect(result).toEqual(142)
  })

  describe('Part 1', () => {
    it('Should return the calibration values of each line', () => {
      const result = calculateCalibrationValuesPt1(inputArrayPt1)
      expect(result).toStrictEqual([12, 38, 15, 77])
    })
  })

  describe('Part 2', () => {
    it('Should return the calibration values of each line', () => {
      const result = calculateCalibrationValuesPt2(inputArrayPt2)
      expect(result).toStrictEqual([29, 83, 13, 24, 42, 14, 76])
    })
  })
})
