import { day01 } from './day01/day01'
import { day02 } from './day02/day02'
import { day03 } from './day03/day03'
import { day04 } from './day04/day04'
import { day05 } from './day05/day05'

export const aoc2023 = [
  { func: day01, label: '2023 - Day 01' },
  { func: day02, label: '2023 - Day 02' },
  { func: day03, label: '2023 - Day 03' },
  { func: day04, label: '2023 - Day 04' },
  { func: day05, label: '2023 - Day 05' },
]
