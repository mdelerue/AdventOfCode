import { Card, computeCardScore, formatCard, part2, processCard } from './day04'

describe('2023 - Day 04', () => {
  const inputs = [
    'Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53',
    'Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19',
    'Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1',
    'Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83',
    'Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36',
    'Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11',
  ]

  it('Should format input', () => {
    expect(formatCard(inputs[0])).toEqual({
      id: 1,
      winningNumbers: [41, 48, 83, 86, 17],
      presentNumbers: [83, 86, 6, 31, 17, 9, 48, 53],
    })
  })

  it('Should get the score of card', () => {
    expect(computeCardScore(formatCard(inputs[0]))).toEqual(8)
    expect(computeCardScore(formatCard(inputs[1]))).toEqual(2)
    expect(computeCardScore(formatCard(inputs[2]))).toEqual(2)
    expect(computeCardScore(formatCard(inputs[3]))).toEqual(1)
    expect(computeCardScore(formatCard(inputs[4]))).toEqual(0)
    expect(computeCardScore(formatCard(inputs[5]))).toEqual(0)
  })

  it('Should process a card and update quantities reference', () => {
    const quantities = { 1: 3, 2: 2, 3: 1, 4: 2 }
    const card: Card = {
      id: 1,
      winningNumbers: [2, 3, 4],
      presentNumbers: [3, 5, 4],
    }

    processCard(card, quantities)
    expect(quantities).toEqual({ 1: 3, 2: 5, 3: 4, 4: 2 })
  })

  it('Should return part 2 result', () => {
    expect(part2(inputs)).toBe(30)
  })
})
