import * as path from 'path'

import fileHelper from '../../helpers/file'

const filepath = path.resolve('src/2023/day04/input.txt')

export type Card = {
  id: number
  winningNumbers: number[]
  presentNumbers: number[]
}

export const formatCard = (input: string): Card => {
  const id = parseInt(input.split(': ')[0].replace('Card ', ''))
  const [winnings, presents] = input.split(': ')[1].split(' | ')
  return {
    id,
    winningNumbers: winnings
      .replaceAll('  ', ' ')
      .split(' ')
      .map((str) => parseInt(str, 10)),
    presentNumbers: presents
      .replaceAll('  ', ' ')
      .split(' ')
      .map((str) => parseInt(str, 10)),
  }
}

export const computeCardScore = (card: Card): number => {
  return card.winningNumbers
    .filter((num) => card.presentNumbers.includes(num))
    .reduce((score, _item, idx) => (idx === 0 ? 1 : score * 2), 0)
}

export const processCard = (card: Card, quantities: Record<string, number>): void => {
  const numberOfWinningNumbers = card.winningNumbers.filter((num) =>
    card.presentNumbers.includes(num),
  ).length

  const cardQuantity = quantities[`${card.id}`]
  for (let i = 1; i <= numberOfWinningNumbers; i++) {
    const wonId = card.id + i
    if (quantities[wonId]) {
      quantities[wonId] = quantities[wonId] + cardQuantity
    }
  }
}

export const part2 = (inputs: string[]): number => {
  const cards = inputs.map((input) => formatCard(input))
  const quantities = cards.reduce(
    (cardQuantity, card) => ({ ...cardQuantity, [`${card.id}`]: 1 }),
    {},
  )

  for (const card of cards) {
    processCard(card, quantities)
  }

  return Object.entries<number>(quantities).reduce((sum, [_id, value]) => sum + value, 0)
}

const part1 = (inputs: string[]): number => {
  return inputs.reduce((sum, input) => sum + computeCardScore(formatCard(input)), 0)
}
export const day04 = async () => {
  const inputs = await fileHelper.readInput(filepath)

  return {
    part1: part1(inputs),
    part2: part2(inputs),
  }
}
