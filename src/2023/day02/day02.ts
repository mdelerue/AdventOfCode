import * as path from 'path'

import fileHelper from '../../helpers/file'

export type Set = {
  red?: number
  blue?: number
  green?: number
}

export type FormattedInput = {
  id: number
  sets: Set[]
}

const filepath = path.resolve('src/2023/day02/input.txt')

const controlValue: Set = {
  red: 12,
  green: 13,
  blue: 14,
}

export const isGameValid = (game: FormattedInput, controlValue: Set): boolean => {
  return game.sets.every(
    ({ red, blue, green }) =>
      (controlValue?.blue ?? 0) >= (blue ?? 0) &&
      (controlValue?.red ?? 0) >= (red ?? 0) &&
      (controlValue?.green ?? 0) >= (green ?? 0),
  )
}

export const getFewestNumber = (sets: Set[]): Set => {
  return sets.reduce(
    (acc: Set, set: Set) => {
      return {
        blue: (set.blue ?? 0) > (acc.blue ?? 0) ? set.blue : acc.blue,
        red: (set.red ?? 0) > (acc.red ?? 0) ? set.red : acc.red,
        green: (set.green ?? 0) > (acc.green ?? 0) ? set.green : acc.green,
      }
    },
    { green: 0, red: 0, blue: 0 },
  )
}

const getPower = (set: Set): number => {
  return (set.blue ?? 0) * (set.green ?? 0) * (set.red ?? 0)
}

export const formatInputs = (input: string[]): FormattedInput[] => {
  return input.map((line) => {
    const [prefix, setsStr] = line.split(': ')

    const sets = setsStr.split('; ').map((setStr) =>
      setStr.split(', ').reduce((set, str) => {
        const [num, color] = str.split(' ')
        return {
          ...set,
          [color]: parseInt(num, 10),
        }
      }, {}),
    )

    return {
      id: parseInt(prefix.replace('Game ', ''), 10),
      sets,
    }
  })
}

export const part1 = (formattedInputs: FormattedInput[]): number => {
  return formattedInputs.reduce(
    (sum, formattedInput) =>
      sum + (isGameValid(formattedInput, controlValue) ? formattedInput.id : 0),
    0,
  )
}

export const part2 = (formattedInputs: FormattedInput[]): number => {
  return formattedInputs.reduce(
    (sum, formattedInput) => sum + getPower(getFewestNumber(formattedInput.sets)),
    0,
  )
}

export const day02 = async () => {
  const input = await fileHelper.readInput(filepath)

  const formattedInputs = formatInputs(input)

  return {
    part1: part1(formattedInputs),
    part2: part2(formattedInputs),
  }
}
