import { FormattedInput, formatInputs, getFewestNumber, isGameValid, part1, part2 } from './day02'

describe('2023 - Day 02', () => {
  let formattedInput: FormattedInput[] = []
  beforeAll(() => {
    formattedInput = formatInputs(input)
  })
  const input = [
    'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green',
    'Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue',
    'Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red',
    'Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red',
    'Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green',
  ]
  const expectedFormattedInput: FormattedInput[] = [
    {
      id: 1,
      sets: [
        {
          blue: 3,
          red: 4,
        },
        {
          red: 1,
          green: 2,
          blue: 6,
        },
        {
          green: 2,
        },
      ],
    },
    {
      id: 2,
      sets: [
        {
          blue: 1,
          green: 2,
        },
        {
          green: 3,
          blue: 4,
          red: 1,
        },
        {
          green: 1,
          blue: 1,
        },
      ],
    },
  ]

  it('Should format the input in an array of FormattedInput', () => {
    const result = formatInputs(input).filter(({ id }) => id <= 2)

    expect(result).toStrictEqual(expectedFormattedInput)
  })

  describe('isGameValid()', () => {
    let controlValue = {
      red: 12,
      green: 13,
      blue: 14,
    }

    it('Should be TRUE for game 1 / 2 / 5', () => {
      expect(isGameValid(formattedInput.find(({ id }) => id === 1)!, controlValue)).toBe(true)
      expect(isGameValid(formattedInput.find(({ id }) => id === 2)!, controlValue)).toBe(true)
      expect(isGameValid(formattedInput.find(({ id }) => id === 3)!, controlValue)).toBe(false)
      expect(isGameValid(formattedInput.find(({ id }) => id === 4)!, controlValue)).toBe(false)
      expect(isGameValid(formattedInput.find(({ id }) => id === 5)!, controlValue)).toBe(true)
    })
  })

  it('Should return part 1 result', () => {
    expect(part1(formattedInput)).toBe(8)
  })

  it('Should return the fewest possible set', () => {
    expect(getFewestNumber(formattedInput.find(({ id }) => id === 1)!.sets)).toEqual({
      red: 4,
      green: 2,
      blue: 6,
    })
    expect(getFewestNumber(formattedInput.find(({ id }) => id === 2)!.sets)).toEqual({
      red: 1,
      green: 3,
      blue: 4,
    })
    expect(getFewestNumber(formattedInput.find(({ id }) => id === 3)!.sets)).toEqual({
      red: 20,
      green: 13,
      blue: 6,
    })
    expect(getFewestNumber(formattedInput.find(({ id }) => id === 4)!.sets)).toEqual({
      red: 14,
      green: 3,
      blue: 15,
    })
    expect(getFewestNumber(formattedInput.find(({ id }) => id === 5)!.sets)).toEqual({
      red: 6,
      green: 3,
      blue: 2,
    })
  })

  it('Should return the part2 result', () => {
    expect(part2(formattedInput)).toBe(2286)
  })
})
