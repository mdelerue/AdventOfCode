import {
  NOT_ASTERIX_REGEX,
  DIGIT_SPACE_REGEX,
  getCoordinatesToBeChecked,
  getPartCoordinates,
  getSymbolCoordinates,
  isPartValid,
  part1,
  getGearRatio,
  formatPartsWithPossibleSymbolCoord,
  part2,
} from './day03'

describe('2023 - Day 03', () => {
  const inputs = [
    '467..114..',
    '...*......',
    '..35..633.',
    '......#...',
    '617*......',
    '.....+.58.',
    '..592.....',
    '......755.',
    '...$.*....',
    '.664.598..',
  ]

  it('Should get all symbol coordinates', () => {
    const result = getSymbolCoordinates(inputs, DIGIT_SPACE_REGEX)
    expect(result).toEqual([
      { x: 3, y: 1 },
      { x: 6, y: 3 },
      { x: 3, y: 4 },
      { x: 5, y: 5 },
      { x: 3, y: 8 },
      { x: 5, y: 8 },
    ])
  })

  it('Should get all part coordinates', () => {
    const result = getPartCoordinates(inputs)
    expect(result).toEqual([
      {
        id: 467,
        coordinates: { x: 0, y: 0 },
      },
      {
        id: 114,
        coordinates: { x: 5, y: 0 },
      },
      {
        id: 35,
        coordinates: { x: 2, y: 2 },
      },
      {
        id: 633,
        coordinates: { x: 6, y: 2 },
      },
      {
        id: 617,
        coordinates: { x: 0, y: 4 },
      },
      {
        id: 58,
        coordinates: { x: 7, y: 5 },
      },
      {
        id: 592,
        coordinates: { x: 2, y: 6 },
      },
      {
        id: 755,
        coordinates: { x: 6, y: 7 },
      },
      {
        id: 664,
        coordinates: { x: 1, y: 9 },
      },
      {
        id: 598,
        coordinates: { x: 5, y: 9 },
      },
    ])
  })

  it('Should return a list of coordinates where to look for symbol', () => {
    const part1Result = getCoordinatesToBeChecked(
      {
        id: 467,
        coordinates: { x: 0, y: 0 },
      },
      2,
    )

    expect(part1Result).toEqual([
      { x: 0, y: -1 },
      { x: 0, y: 1 },
      { x: 1, y: -1 },
      { x: 1, y: 1 },
      { x: 2, y: -1 },
      { x: 2, y: 1 },
    ])

    const part2Result = getCoordinatesToBeChecked(
      {
        id: 4,
        coordinates: { x: 5, y: 1 },
      },
      10,
    )

    expect(part2Result).toStrictEqual([
      { x: 4, y: 0 },
      { x: 4, y: 1 },
      { x: 4, y: 2 },
      { x: 5, y: 0 },
      { x: 5, y: 2 },
      { x: 6, y: 0 },
      { x: 6, y: 1 },
      { x: 6, y: 2 },
    ])
  })

  it('Should check for valid part', () => {
    const symbolCoordinates = getSymbolCoordinates(inputs, DIGIT_SPACE_REGEX)
    const maxX = 9

    expect(
      isPartValid(
        {
          id: 467,
          coordinates: { x: 0, y: 0 },
        },
        symbolCoordinates,
        maxX,
      ),
    ).toBe(true)

    expect(
      isPartValid(
        {
          id: 114,
          coordinates: { x: 5, y: 0 },
        },
        symbolCoordinates,
        maxX,
      ),
    ).toBe(false)

    expect(isPartValid({ id: 58, coordinates: { x: 7, y: 5 } }, symbolCoordinates, maxX)).toBe(
      false,
    )
  })

  it('Should return the part1 result', () => {
    expect(part1(inputs)).toBe(4361)
  })

  it('Should get all the asterisk coordinates', () => {
    const result = getSymbolCoordinates(inputs, NOT_ASTERIX_REGEX)
    expect(result).toEqual([
      { x: 3, y: 1 },
      { x: 3, y: 4 },
      { x: 5, y: 8 },
    ])
  })

  it('Should check if a gear is valid and return its ratio', () => {
    const parts = formatPartsWithPossibleSymbolCoord(getPartCoordinates(inputs), inputs[0].length)
    expect(getGearRatio({ x: 3, y: 1 }, parts)).toEqual(16345)
    expect(getGearRatio({ x: 3, y: 4 }, parts)).toEqual(0)
    expect(getGearRatio({ x: 5, y: 8 }, parts)).toEqual(451490)
  })

  it('Should return the part2 result', () => {
    expect(part2(inputs)).toEqual(467835)
  })
})
