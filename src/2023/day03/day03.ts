import * as path from 'path'

import fileHelper from '../../helpers/file'

const filepath = path.resolve('src/2023/day03/input.txt')
export const DIGIT_SPACE_REGEX = /(\d|\s)/
export const NOT_ASTERIX_REGEX = /([^\*])/

export type Coordinates = {
  x: number
  y: number
}

export type PartCoordinates = {
  id: number
  coordinates: Coordinates
}

export const getSymbolCoordinates = (inputs: string[], regex: RegExp): Coordinates[] => {
  return inputs.reduce((coordinates: Coordinates[], input, currentY) => {
    const lineCoordinates = input
      .replaceAll('.', ' ')
      .split('')
      .reduce((acc, char, idx) => {
        if (regex.test(char)) {
          return acc
        }
        return [...acc, { y: currentY, x: idx }]
      }, [] as Coordinates[])

    return [...coordinates, ...lineCoordinates]
  }, [])
}

export const getPartCoordinates = (inputs: string[]): PartCoordinates[] => {
  return inputs.reduce(
    (coordinates: PartCoordinates[], input, currentY) => [
      ...coordinates,
      ...extractPartFromLine(input).map(({ numberGroup, startIndex }) => ({
        id: parseInt(numberGroup, 10),
        coordinates: { x: startIndex, y: currentY },
      })),
    ],

    [],
  )
}

const extractPartFromLine = (input: string): { numberGroup: string; startIndex: number }[] => {
  const result: { numberGroup: string; startIndex: number }[] = []
  let currentNumberGroup = ''

  for (let i = 0; i < input.length; i++) {
    const char = input[i]

    if (/\d/.test(char)) {
      currentNumberGroup = `${currentNumberGroup}${char}`
      if (currentNumberGroup.length === 1) {
        result.push({ numberGroup: currentNumberGroup, startIndex: i })
        continue
      }

      result[result.length - 1].numberGroup = `${result[result.length - 1].numberGroup}${char}`
    } else {
      currentNumberGroup = ''
    }
  }

  return result
}

export const getCoordinatesToBeChecked = (
  partCoordinates: PartCoordinates,
  maxX: number,
): Coordinates[] => {
  const idStr = `${partCoordinates.id}`
  const {
    coordinates: { x, y },
  } = partCoordinates

  const coordinates: Coordinates[] = []

  if (x > 0) {
    coordinates.push({ x: x - 1, y: y - 1 }, { x: x - 1, y }, { x: x - 1, y: y + 1 })
  }

  coordinates.push(
    ...idStr.split('').reduce((acc: Coordinates[], _char, idx) => {
      return [...acc, { x: x + idx, y: y - 1 }, { x: x + idx, y: y + 1 }]
    }, []),
  )

  if (x + idStr.length <= maxX) {
    coordinates.push(
      { x: x + idStr.length, y: y - 1 },
      { x: x + idStr.length, y },
      { x: x + idStr.length, y: y + 1 },
    )
  }

  return coordinates
}

export const isPartValid = (
  part: PartCoordinates,
  symbolCoordinates: Coordinates[],
  maxX: number,
): boolean => {
  const toBeChecked = getCoordinatesToBeChecked(part, maxX)
  const symbolCoordinate = toBeChecked.find(({ x, y }) =>
    symbolCoordinates.some(({ x: symbolX, y: symbolY }) => x === symbolX && y === symbolY),
  )
  return symbolCoordinate !== undefined
}

export const part1 = (inputs: string[]): number => {
  const symbolCoordinates = getSymbolCoordinates(inputs, DIGIT_SPACE_REGEX)
  const partCoordinates = getPartCoordinates(inputs)
  const maxX = inputs[0].length

  return partCoordinates.reduce(
    (sum, partCoordinate) =>
      sum + (isPartValid(partCoordinate, symbolCoordinates, maxX) ? partCoordinate.id : 0),
    0,
  )
}

export const formatPartsWithPossibleSymbolCoord = (
  parts: PartCoordinates[],
  maxX: number,
): Array<PartCoordinates & { toBeChecked: Coordinates[] }> => {
  return parts.map((part) => ({ ...part, toBeChecked: getCoordinatesToBeChecked(part, maxX) }))
}
export const getGearRatio = (
  coordinates: Coordinates,
  parts: Array<PartCoordinates & { toBeChecked: Coordinates[] }>,
): number => {
  const adjacentPart = parts.filter(({ toBeChecked }) =>
    toBeChecked.some(({ x, y }) => x === coordinates.x && y === coordinates.y),
  )

  return adjacentPart.length === 2 ? adjacentPart.reduce((product, { id }) => id * product, 1) : 0
}

export const part2 = (inputs: string[]): number => {
  const maxX = inputs[0].length
  const asterisks = getSymbolCoordinates(inputs, NOT_ASTERIX_REGEX)
  const parts = formatPartsWithPossibleSymbolCoord(getPartCoordinates(inputs), maxX)

  return asterisks.reduce((sum, asterisk) => sum + getGearRatio(asterisk, parts), 0)
}

export const day03 = async () => {
  const inputs = await fileHelper.readInput(filepath)

  return {
    part1: part1(inputs),
    part2: part2(inputs),
  }
}
