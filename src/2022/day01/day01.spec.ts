import { calculateMaxCalories, calculateTopSum } from './day01'

const inputArray = [[1000, 2000, 3000], [4000], [5000, 6000], [7000, 8000, 9000], [10000]]

describe('2022 - Day 01', () => {
  it('Should return 24000 (calories carried by the fourth elve)', () => {
    const result = calculateMaxCalories(inputArray)
    expect(result).toBe(24000)
  })

  it('Should return the sum of the top three carrier', () => {
    const result = calculateTopSum(inputArray, 3)
    expect(result).toBe(45000)
  })
})
