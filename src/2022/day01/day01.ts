import fileHelper from '../../helpers/file'
import * as path from 'path'

const filepath = path.resolve('src/2022/day01/input.txt')

export const calculateTopSum = (input: number[][], numberOfElementsToKeep: number): number => {
  const sums = input.reduce((results, elve: number[]) => {
    return [...results, elve.reduce((total, calories) => total + calories, 0)]
  }, [] as number[])

  sums.sort((a, b) => b - a)

  let result = 0
  for (let i = 0; i < numberOfElementsToKeep; i++) {
    result += sums[i]
  }

  return result
}

export const calculateMaxCalories = (input: number[][]): number => {
  return input.reduce((max, elve: number[]) => {
    const sum = elve.reduce((total, calories) => total + calories, 0)
    if (sum > max) {
      return sum
    }
    return max
  }, 0)
}

export const day01 = async () => {
  const input = (await fileHelper.readInput(filepath)).reduce(
    (groups, line) => {
      const value = Number(line)

      if (!value) {
        return [...groups, []]
      }
      const current = groups[groups.length - 1]
      const previousGroups = groups.slice(0, -1)
      return [...previousGroups, [...current, value]]
    },
    [[]] as number[][],
  )

  return {
    part1: calculateMaxCalories(input),
    part2: calculateTopSum(input, 3),
  }
}
