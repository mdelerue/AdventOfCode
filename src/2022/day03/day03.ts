import fileHelper from '../../helpers/file'
import * as path from 'path'

const filepath = path.resolve('src/2022/day03/input.txt')
const initialDecreaseLower = 96
const initialDecreaseUpper = 38
const lowerRegex = new RegExp(/[a-z]/)

const getChunks = <T>(array: T[], chunkSize: number): T[][] => {
  const temporal = []

  for (let i = 0; i < array.length; i += chunkSize) {
    temporal.push(array.slice(i, i + chunkSize))
  }

  return temporal
}

export const getPriority = (letter: string): number => {
  const isLower = lowerRegex.test(letter)

  return letter.charCodeAt(0) - (isLower ? initialDecreaseLower : initialDecreaseUpper)
}

export const getElementsInCommonsByBag = (bagItems: string): string[] => {
  const bagContent = bagItems.split('')
  const middleIndex = Math.ceil(bagContent.length / 2)

  const compartment1 = bagContent.splice(0, middleIndex)
  const compartment2 = bagContent.splice(-middleIndex)

  return compartment1.reduce((duplicates, item) => {
    if (!duplicates.includes(item) && compartment2.includes(item)) {
      return [...duplicates, item]
    }

    return duplicates
  }, [] as string[])
}

export const getBadge = (group: string[]): string => {
  const groupContent = group.map((bag) => bag.split(''))
  if (groupContent.length !== 3) {
    return ''
  }

  return groupContent[0].reduce((badge: string, item) => {
    if (!badge && groupContent[1].includes(item) && groupContent[2].includes(item)) {
      return item
    }
    return badge
  }, '')
}

export const part1 = (bags: string[]): number => {
  const commonsItem = bags
    .reduce((commons, bag) => [...commons, getElementsInCommonsByBag(bag)], [] as string[][])
    .flat()
  return commonsItem.reduce((sum, item) => sum + getPriority(item), 0)
}

export const part2 = (bags: string[]): number => {
  return getChunks(bags, 3).reduce((sum, group) => {
    return sum + getPriority(getBadge(group))
  }, 0)
}

export const day03 = async () => {
  const input = await fileHelper.readInput(filepath)

  return {
    part1: part1(input),
    part2: part2(input),
  }
}
