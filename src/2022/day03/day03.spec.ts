import { getBadge, getElementsInCommonsByBag, getPriority, part1, part2 } from './day03'

const inputArray = [
  'vJrwpWtwJgWrhcsFMMfFFhFp',
  'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
  'PmmdzqPrVvPwwTWBwg',
  'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
  'ttgJtRGJQctTZtZT',
  'CrZsJsPPZsGzwwsLwLmpwMDw',
]

const inputGroup1 = [
  'vJrwpWtwJgWrhcsFMMfFFhFp',
  'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
  'PmmdzqPrVvPwwTWBwg',
]

const inputGroup2 = [
  'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
  'ttgJtRGJQctTZtZT',
  'CrZsJsPPZsGzwwsLwLmpwMDw',
]

describe('2022 - Day 03', () => {
  it('Should return the elements in commons in each compartment of each bag', () => {
    const result = inputArray.map((bag) => getElementsInCommonsByBag(bag))
    expect(result).toStrictEqual([['p'], ['L'], ['P'], ['v'], ['t'], ['s']])
  })

  it('Should get the priority of items', () => {
    const result = ['p', 'L', 'P', 'v', 't', 's'].map((letter) => getPriority(letter))
    expect(result).toStrictEqual([16, 38, 42, 22, 20, 19])
  })

  it('Should return the sum of priorities for part1', () => {
    const result = part1(inputArray)
    expect(result).toBe(157)
  })

  it('Should return the only item in common in all 3 bags of group 1', () => {
    const result = getBadge(inputGroup1)
    expect(result).toBe('r')
  })

  it('Should return the only item in common in all 3 bags of group 2', () => {
    const result = getBadge(inputGroup2)
    expect(result).toBe('Z')
  })

  it('Should return the sum of priorities for badges', () => {
    const result = part2([...inputGroup1, ...inputGroup2])
    expect(result).toBe(70)
  })
})
