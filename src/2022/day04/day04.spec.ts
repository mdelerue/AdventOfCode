import {
  containsRange,
  numberOfFullyContainedRange,
  numberOfOverlappingRange,
  overlaps,
  parseLine,
} from './day04'

const inputArray = ['2-4,6-8', '2-3,4-5', '5-7,7-9', '2-8,3-7', '6-6,4-6', '2-6,4-8']

describe('2022 - Day 04', () => {
  it('Should return true because the first range contains the second one', () => {
    const result = containsRange({
      range1: [2, 5],
      range2: [4, 4],
    })

    expect(result).toBeTruthy()
  })

  it('Should return true because the second range contains the first one', () => {
    const result = containsRange({
      range1: [2, 5],
      range2: [2, 6],
    })

    expect(result).toBeTruthy()
  })

  it('Should return false because no range contains the other', () => {
    const result = containsRange({
      range1: [2, 5],
      range2: [4, 6],
    })

    expect(result).toBeFalsy()
  })

  it('Should return the number of range fully contained', () => {
    const result = numberOfFullyContainedRange([
      {
        range1: [2, 5],
        range2: [2, 6],
      },
      {
        range1: [2, 5],
        range2: [4, 4],
      },
      {
        range1: [2, 5],
        range2: [4, 6],
      },
    ])

    expect(result).toBe(2)
  })

  it('Should return a RangeWrapper', () => {
    const result = parseLine('2-4,6-8')
    expect(result).toEqual({
      range1: [2, 4],
      range2: [6, 8],
    })
  })

  it('Should return the number of fully contained in the example input', () => {
    const result = numberOfFullyContainedRange(inputArray.map((line) => parseLine(line)))
    expect(result).toBe(2)
  })

  it('Should return true because the ranges are overlapping', () => {
    const result = overlaps({
      range1: [2, 5],
      range2: [4, 6],
    })

    expect(result).toBeTruthy()
  })

  it('Should return true because the ranges are overlapping', () => {
    const result = overlaps({
      range1: [4, 6],
      range2: [2, 5],
    })

    expect(result).toBeTruthy()
  })

  it('Should return the number of overlapping ranges in the example input', () => {
    const result = numberOfOverlappingRange(inputArray.map((line) => parseLine(line)))
    expect(result).toBe(4)
  })
})
