import fileHelper from '../../helpers/file'
import * as path from 'path'

const filepath = path.resolve('src/2022/day04/input.txt')

export type RangeWrapper = {
  range1: number[]
  range2: number[]
}

export const containsRange = (ranges: RangeWrapper): boolean => {
  const { range1, range2 } = ranges

  return (
    (range1[0] >= range2[0] && range1[1] <= range2[1]) ||
    (range2[0] >= range1[0] && range2[1] <= range1[1])
  )
}

export const overlaps = (ranges: RangeWrapper): boolean => {
  const { range1, range2 } = ranges

  return (
    (range1[0] >= range2[0] && range1[0] <= range2[1]) ||
    (range2[0] >= range1[0] && range2[0] <= range1[1])
  )
}

export const numberOfFullyContainedRange = (ranges: RangeWrapper[]): number => {
  return ranges.reduce((sum, range) => sum + (containsRange(range) ? 1 : 0), 0)
}

export const numberOfOverlappingRange = (ranges: RangeWrapper[]): number => {
  return ranges.reduce((sum, range) => sum + (overlaps(range) ? 1 : 0), 0)
}

export const parseLine = (line: string): RangeWrapper => {
  const [r1, r2] = line.split(',')

  return {
    range1: r1.split('-').map((item) => Number(item)),
    range2: r2.split('-').map((item) => Number(item)),
  }
}

export const day04 = async () => {
  const input = await fileHelper.readInput(filepath)

  return {
    part1: numberOfFullyContainedRange(input.map((line) => parseLine(line))),
    part2: numberOfOverlappingRange(input.map((line) => parseLine(line))),
  }
}
