import fileHelper from '../../helpers/file'
import * as path from 'path'

const filepath = path.resolve('src/2022/day06/input.txt')

export const findFirstDifferentCharEndIndex = (input: string, nbChar: number): number => {
  const arr = input.split('')

  let endIndex = 0

  for (let i = nbChar; i <= arr.length; i++) {
    const excerpt = arr.slice(i - nbChar, i)
    const set = new Set(excerpt)

    if (set.size === nbChar) {
      endIndex = i
      break
    }
  }

  return endIndex
}

export const day06 = async () => {
  const [input] = await fileHelper.readInput(filepath)

  return {
    part1: findFirstDifferentCharEndIndex(input, 4),
    part2: findFirstDifferentCharEndIndex(input, 14),
  }
}
