import { findFirstDifferentCharEndIndex } from './day06'

describe('2022 - Day 06', () => {
  const input = 'mjqjpqmgbljsphdztnvjfqwrcgsmlb'
  const marker = 7

  it('Should find first 4 different chars end index', () => {
    const result = findFirstDifferentCharEndIndex(input, 4)
    expect(result).toBe(marker)
  })
})
