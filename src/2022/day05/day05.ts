import fileHelper from '../../helpers/file'
import * as path from 'path'

export type Position = {
  [key: number]: string[]
}

export type Instruction = {
  from: number
  to: number
  quantity: number
}

const filepath = path.resolve('src/2022/day05/input.txt')

const parseInstructionLineRegex = /^move (\d*) from (\d*) to (\d*)$/gm

export const executeInstruction = (instruction: Instruction, position: Position): Position => {
  let newPosition = { ...position }
  const { to, from, quantity } = instruction

  for (let i = 1; i <= quantity; i++) {
    newPosition[to] = [...newPosition[from].splice(0, 1), ...newPosition[to]]
  }

  return newPosition
}

export const executeInstructionPt2 = (instruction: Instruction, position: Position): Position => {
  let newPosition = { ...position }
  const { to, from, quantity } = instruction

  newPosition[to] = [...newPosition[from].splice(0, quantity), ...newPosition[to]]

  return newPosition
}

const parseInstructionLine = (line: string): Instruction | null => {
  const groups = line.matchAll(parseInstructionLineRegex)

  for (const group of groups) {
    return { quantity: Number(group[1]), from: Number(group[2]), to: Number(group[3]) }
  }

  return null
}

export const parseInput = (
  input: string[],
): { initialPosition: Position; instructions: Instruction[] } => {
  const initialLineRegex = /(.{4})/gm

  return input.reduce(
    (acc: { instructions: Instruction[]; initialPosition: Position }, line) => {
      if (line === '') {
        return acc
      }

      if (line.includes('move')) {
        const instruction = parseInstructionLine(line)
        return {
          ...acc,
          instructions: instruction ? [...acc.instructions, instruction] : acc.instructions,
        }
      }

      if (line.includes('[')) {
        // shame !!
        const linePadded = line.padEnd(line.length + (4 - (line.length % 4)), ' ')
        const capturedGroups = [...linePadded.matchAll(initialLineRegex)]

        let initArray = acc.initialPosition

        capturedGroups.forEach((value: RegExpMatchArray, idx) => {
          const column = Number(value.index) / 4 + 1
          const char = value
            .values()
            .next()
            .value.replace(/[\[\] ]/gm, '')

          if (!initArray[column]) {
            initArray[column] = []
          }

          if (char) {
            initArray[column] = [...initArray[column], char]
          }
        })

        return { ...acc, initialPosition: { ...initArray } }
      }

      return acc
    },
    { instructions: [], initialPosition: {} } as {
      instructions: Instruction[]
      initialPosition: Position
    },
  )
}

export const part1 = (input: string[]): string => {
  const { instructions, initialPosition } = parseInput(input)

  const finalPosition = instructions.reduce(
    (position, instruction) => executeInstruction(instruction, position),
    initialPosition,
  )

  return Object.values(finalPosition)
    .map((stack) => stack.slice(0, 1))
    .join('')
}

export const part2 = (input: string[]): string => {
  const { instructions, initialPosition } = parseInput(input)

  const finalPosition = instructions.reduce(
    (position, instruction) => executeInstructionPt2(instruction, position),
    initialPosition,
  )

  return Object.values(finalPosition)
    .map((stack) => stack.slice(0, 1))
    .join('')
}

export const day05 = async () => {
  const input = await fileHelper.readInput(filepath)

  return {
    part1: part1(input),
    part2: part2(input),
  }
}
