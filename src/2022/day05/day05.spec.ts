import { executeInstruction, parseInput, part1, part2, Position } from './day05'

describe('2022 - Day 05', () => {
  const input = [
    '    [D]    ',
    '[N] [C]    ',
    '[Z] [M] [P]',
    ' 1   2   3 ',
    '',
    'move 1 from 2 to 1',
    'move 3 from 1 to 3',
    'move 2 from 2 to 1',
    'move 1 from 1 to 2',
  ]

  const expectedInitialPosition: Position = {
    1: ['N', 'Z'],
    2: ['D', 'C', 'M'],
    3: ['P'],
  }

  const expectedPositionAfterInstruction1: Position = {
    1: ['D', 'N', 'Z'],
    2: ['C', 'M'],
    3: ['P'],
  }

  const expectedPositionAfterInstruction2: Position = {
    1: [],
    2: ['C', 'M'],
    3: ['Z', 'N', 'D', 'P'],
  }

  const expectedInstruction = [
    { quantity: 1, from: 2, to: 1 },
    { quantity: 3, from: 1, to: 3 },
    { quantity: 2, from: 2, to: 1 },
    { quantity: 1, from: 1, to: 2 },
  ]

  it('Should parse the input file', () => {
    const { initialPosition, instructions } = parseInput(input)

    expect(initialPosition).toEqual(expectedInitialPosition)
    expect(instructions).toEqual(expectedInstruction)
  })

  it('Should apply instruction 1', () => {
    const result = executeInstruction(expectedInstruction[0], expectedInitialPosition)
    expect(result).toEqual(expectedPositionAfterInstruction1)
  })

  it('Should apply instruction 1 and 2', () => {
    const result = expectedInstruction.slice(0, 2).reduce((position, instruction) => {
      return executeInstruction(instruction, position)
    }, expectedInitialPosition)

    expect(result).toEqual(expectedPositionAfterInstruction2)
  })

  it('[Part 1] Should get the top crate at the final position', () => {
    const result = part1(input)

    expect(result).toBe('CMZ')
  })

  it('[Part 1] Should get the top crate at the final position', () => {
    const result = part2(input)

    expect(result).toBe('MCD')
  })
})
