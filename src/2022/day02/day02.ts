import fileHelper from '../../helpers/file'
import * as path from 'path'

const filepath = path.resolve('src/2022/day02/input.txt')

type shape = 'X' | 'Y' | 'Z'
const shapesMapping: { inputShape: shape; match: string; value: number; defeat: string }[] = [
  {
    inputShape: 'X',
    match: 'A',
    value: 1,
    defeat: 'C',
  },
  {
    inputShape: 'Y',
    match: 'B',
    value: 2,
    defeat: 'A',
  },
  {
    inputShape: 'Z',
    match: 'C',
    value: 3,
    defeat: 'B',
  },
]

const WIN_POINT = 6
const DRAW_POINT = 3

const calculateRoundScorePt2 = (opponentShape: shape, playerShape: shape): number => {
  let playing
  let bonus = 0

  if (playerShape === 'X') {
    // have to loose
    playing = shapesMapping.find(
      ({ match: needed }) =>
        needed === shapesMapping.find(({ match }) => match === opponentShape)?.defeat,
    )
  } else if (playerShape === 'Y') {
    // have to draw
    playing = shapesMapping.find(({ match }) => match === opponentShape)
    bonus = DRAW_POINT
  } else {
    // win !!
    playing = shapesMapping.find(({ defeat }) => defeat === opponentShape)
    bonus = WIN_POINT
  }

  return playing!.value + bonus
}

const calculateRoundScore = (opponentShape: shape, playerShape: shape): number => {
  const playedShape = shapesMapping.find(({ inputShape }) => inputShape === playerShape)

  if (!playedShape) {
    return 0
  }

  return (
    playedShape.value +
    (playedShape.match === opponentShape
      ? DRAW_POINT
      : opponentShape === playedShape.defeat
        ? WIN_POINT
        : 0)
  )
}

export const calculateTotalScore = (input: string[][], part2 = false): number => {
  return input.reduce(
    (sum, roundShapes) =>
      sum +
      (part2
        ? calculateRoundScorePt2(roundShapes[0] as shape, roundShapes[1] as shape)
        : calculateRoundScore(roundShapes[0] as shape, roundShapes[1] as shape)),
    0,
  )
}

export const day02 = async () => {
  const input = (await fileHelper.readInput(filepath)).map((line) => line.split(' '))

  return {
    part1: calculateTotalScore(input),
    part2: calculateTotalScore(input, true),
  }
}
