import { calculateTotalScore } from './day02'

const inputArray = [
  ['A', 'Y'],
  ['B', 'X'],
  ['C', 'Z'],
]

describe('2022 - Day 02', () => {
  it('Should return the total score of the 3 rounds (15)', () => {
    const result = calculateTotalScore(inputArray)
    expect(result).toBe(15)
  })

  it('Pt 2 : Should return the total score of the 3 rounds (12)', () => {
    const result = calculateTotalScore(inputArray, true)
    expect(result).toBe(12)
  })
})
