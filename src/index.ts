import { loggerEnd, loggerStart } from './helpers/logger'

import { aoc2022 } from './2022/index'
import { aoc2023 } from './2023/index'

const launch = async (call: CallableFunction, label: string) => {
  loggerStart(label)
  const result = await call()
  loggerEnd(label, result)
}

const main = async (years: string[]) => {
  const problems = years
    .map((year) => {
      switch (year) {
        case '2022':
          return aoc2022
        case '2023':
          return aoc2023

        default:
          return []
      }
    })
    .reduce((acc, current) => acc.concat(current), [])

  for await (const problem of problems) {
    const { func, label } = problem
    await launch(func, label)
  }
}

main(process.argv.slice(2))
