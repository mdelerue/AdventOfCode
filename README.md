## Advent of Code

My solutions for [Advent of Code](https://adventofcode.com) problems.

Running using `NodeJS 14.15.1`.

Can be run with `npm run start <year>`.
One or several years as argument separated by spaces.

